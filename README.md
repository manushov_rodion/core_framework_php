# core_framework_php
```
* Базовое самописное ядро для проектов.
* Оно заточено как под сайт, так и под API.
```
* Версия: 1.0.3
* Автор: Manushov Rodion (Манушов Родион) manushov_rodion
* Ссылка: https://vk.com/manushov_rodion
* Email: rodion-krox@mail.ru

## Функционал:
* Авторизация через сессии или через http заголовок
* Кеширование запросов и компонентов
* Локализация, через суб домены
* REST API
* Маршрутизация
* Работа с SSL
* Обработка изображений PNG, JPG и FIG
* Конвертация русских букв на английские, для url, названий и названий файлов

## Внимание!
* папку db не публиковать!
* файл README не публиковать!
* файл .gitignore не публиковать!

## System
* php: ^7.1
* PostgreSQL: ^9.6

#updates

### update 1.0.1
```
* Дал возможность указывать несколько заголовков в контроллере. 
    - $this->view->setHttpHeader( string $key, string $value ) : void

* Дал возможность указывать кастомные значения для вывода результатов. 
    - $this->view->setHttpBody( stdClass|string $storage = null, bool $off_cache = false ) : void
```

### update 1.0.2
```
* Перенес в отдельную папку утилиты и добавил префикс, для удобства поиска
    - system\library\Utilites\Validations -> system\utilites\UtiliteValidations;
    
* Добавил валидацию на адреса запросов:
    - UtiliteValidations::isUrlPathRequest($value, string $key = null) : bool
    - UtiliteValidations::isUrlRequest($value, string $key = null) : bool

* Добавил метод, который заменяет шаблон по умолчанию на пользовательский. Примечание: Название указывается без префикса View.
    - $this->view->setTemplateName(string $template_name) : void

* Добавил утилиту, которая конвертирует русские буквы на английские.
    - UtiliteCyrToLat::name(string $name) : string
    - UtiliteCyrToLat::fileName(string $name) : string
    - UtiliteCyrToLat::utl(string $name) : string

* Добавил утилиту, по работе с изображениями.
    - Метод: UtiliteImage::resize(string $file_name, int $width_user_px, int $height_user_px) : string;
```

### update 1.0.3
```
* Fix обработки маршрутов, с помощью роутера:
    - стали доступны адреса, в аргументах запроса, типа: demo-demo

* В кеше шаблона теперь сохраняются: IP и AGENT пользователя, который первый раз обратился к странице кеша.

* Добавил Утилиту логирования данных, с двумя интерфейсами:
    - UtiliteLog::set(string $file_name, string $message) : void
    - UtiliteLog::setInfoUserConnect(string $note = null) : void

* Добавил блокировка пользователя по IP и AGENT. Необходимо указать IP`s и AGENT`s в файле настроек.
    - Для IP: $_['blocking']['ips'] = ['', '', ...];
    - Для AGENT: $_['blocking']['agents'] = ['', '', ...];
```

### update 1.0.4
```
* Fix проверки наличия добавленного ID сессии.

* Добавлена утилита генерации сложных строк, с методами:
    - UtiliteRandom::randomSecretKey(int $length = 32) : string
    - UtiliteRandom::getHash(string $data, string $secret_key, string $alg = 'sha256') : string

* Обновлена генерация секретных ключей у авторизации
```
