<?php

namespace system\library\DataBase;

/**
 * PDOConnect class
 * - Класс, который содержит методы запроса к БД.
 * 
 * @author Manushov Rodion <rodion-krox@mail.ru>
 * @version 1.0.0
 */
class PDORequest
{
    /**
     * Содержит подключение к БД
     *
     * @var PDO
     */
    public $connect = NULL;

    /**
     * Содержит превикс таблицы бд
     *
     * @var string
     */
    public $prefix_table = '';

    final public function __construct()
    {
        $this->connect = PDOConnect::$connect;

        if (defined('DB_PREFIX')) {
            $this->prefix_table = DB_PREFIX;
        }
    }

    /**
     * Метод, который выполняет запрос к бд и возвращает обьект с методами [row, rows, error, sql]
     *
     * @param string $query
     * @param array $params
     * @return stdClass
     */
    final public function query(string $query, array $params = []) : \stdClass
    {
        $res = new \stdClass();

        if ($this->connect) {
            $query = $this->connect->prepare($query);

            if ($params) {
                $count = count($params);
                for ($i = 0; $i < $count; $i++) {
                    $query->bindParam($i+1, $params[$i], \PDO::PARAM_STR);
                }
            }

            $sql = $query;
            $query->execute();
            $data = $query->fetchAll(\PDO::FETCH_OBJ);

            $res->row = isset($data[0]) ? (object) $data[0] : [];
            $res->rows = $data;
            $res->error = $query->errorInfo();
            $res->sql = $sql;
        }

        return $res;
    }
}