<?php

namespace system\library\Auth\Adapters;

use system\library\Auth\ModifiedJWT\EncodeToken;
use system\library\DataBase\PDORequest;
use system\library\Auth\ModifiedJWT\DecodeToken;


/**
 * AdapterAuthHttp class
 * - Адаптер авторизации, по http.
 * 
 * @author Manushov Rodion <rodion-krox@mail.ru>
 * @version 1.0.0
 */
class AdapterAuthHttp extends Abstract__AuthAdapter
{
    /**
     * Инициализирует авторизацию
     *
     * @return void
     */
    public function initAuth() : void
    {
        if ($this->token) {
            if ($this->db_active === true) {
                
                $jwt = new DecodeToken();
                $jwt->token = str_replace('Bearer ', '', $this->token);;
                $jwt->init();
                if ($jwt->payload) {
                    $this->payload = $jwt->payload;
                    $this->token = $jwt->token;
                    $this->status = true;
                }
            } else {
                trigger_error('Чтобы работала авторизация - необходимо активировать базу данных!', E_USER_ERROR);
            }
        }

        if ($this->status == false) {
            $this->http_headers = [
                'HTTP/1.1' => '401 Unauthorized'
            ];

            $this->http_body = json_encode(['error' => $this->token]);
        }
    }

    /**
     * Метод, который фиксирует авторизацию
     *
     * @return void
     */
    public function setAuth(string $user_id) : void
    {
        if ($this->db_active === true) {
            $jtw = new EncodeToken();
            $jtw->user_id = $user_id;
            $jtw->exp = time() + $this->getOptions()['expire'];
            $jtw->init();
            $this->token = $jtw->token;
        } else {
            trigger_error('Чтобы работала авторизация - необходимо активировать базу данных!', E_USER_ERROR);
        }
    }

    /**
     * Метод, который удаляет авторизацию
     *
     * @param string $user_id
     * @return void
     */
    public function deleteAuth(string $user_id) : void
    {
        $db = new PDORequest();
        $db->query('DELETE FROM xyz_session_auth WHERE exp < ?', [time()]);
        $db->query('DELETE FROM xyz_session_auth WHERE user_id = ?', [$user_id]);
    }
}