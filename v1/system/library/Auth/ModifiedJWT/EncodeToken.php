<?php

namespace system\library\Auth\ModifiedJWT;


use system\library\DataBase\PDORequest;


/**
 * DecodeToken class
 * - Класс, который создает токен
 * 
 * @author Manushov Rodion <rodion-krox@mail.ru>
 * @version 1.0.0
 */
class EncodeToken extends Token
{
    /**
     * Содержит сгенерированный токен
     *
     * @var string
     */
    public $token = '';

    /**
     * Содержит ID пользователя
     *
     * @var string
     */
    public $user_id = '';

    /**
     * Время жизни токена
     *
     * @var string
     */
    public $exp = '';

    final public function init() : void
    {
        $secretKey = $this->randomSecretKey();

        // header
        $header = [
            'typ' => $this->typ,
            'alg' => 'NONE'
        ];
        $header64 = $this->base64Encode(json_encode($header));

        // payload
        $db = new PDORequest();
        $db->query('DELETE FROM '. DB_PREFIX .'session_auth WHERE exp < ?', [time()]);

        $query = $db->query('INSERT INTO '. DB_PREFIX .'session_auth (user_id, token, agent, key, ip, exp) VALUES (?, ?, ?, ?, ?, ?) RETURNING id', [
            $this->user_id, 'tmp', $_SERVER['HTTP_USER_AGENT'],
            $secretKey, $_SERVER['REMOTE_ADDR'],
            $this->exp
        ]);

        if (!$query->row || !$query->row->id) {
            return;
        }

        $payload = [
            'id'        => $query->row->id,
            'exp'       => $this->exp,
            'user_id'   => $this->user_id
        ];
        $payload64 = $this->base64Encode(json_encode($payload));

        // signature
        $signature = [
            'alg' => $this->alg,
            'device' => $_SERVER['HTTP_USER_AGENT']
        ];
        $signature64 = $this->base64Encode(json_encode($signature));

        $hash = $this->getHashHmac([$header64, $payload64, $signature64], $secretKey);
        $hash64 = $this->base64Encode($hash);

        $jwt = implode('.', [$header64, $payload64, $hash64 ]);
        $token = $this->base64Encode($jwt);

        $this->token = $token;
        $db->query('UPDATE '. DB_PREFIX .'session_auth SET token = ? WHERE id=?', [$token, $query->row->id]);
    }
}