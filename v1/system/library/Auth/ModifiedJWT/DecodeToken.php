<?php

namespace system\library\Auth\ModifiedJWT;

use system\utilites\UtiliteValidations;
use system\library\DataBase\PDORequest;


/**
 * DecodeToken class
 * - Класс, который распознает токен, запрошенный на сервере
 * 
 * @author Manushov Rodion <rodion-krox@mail.ru>
 * @version 1.0.0
 */
class DecodeToken extends Token
{
    /**
     * Содержит сгенерированный токен
     *
     * @var string
     */
    public $token = '';

    /**
     * Открытый контент
     * 
     * @var array
     */
    public $payload = [];

    final public function init() : void
    {
        $jwt = $this->base64Decode($this->token);

        $chunks = explode('.', $jwt);
        if (count($chunks) !== 3) {
            return;
        }

        [$header64, $payload64, $hash_token] = $chunks;
        
        $header = json_decode($this->base64Decode($header64));
        if (json_last_error() == JSON_ERROR_NONE) {
            if ($header->alg !== $this->alg && $header->typ !== $this->typ) {
                return;
            }
        } else {
            return;
        }

        $payload = json_decode($this->base64Decode($payload64));
        if (!UtiliteValidations::isUUID($payload->user_id)) {
            return;
        }

        if (!UtiliteValidations::isUUID($payload->id)) {
            return;
        }

        $db = new PDORequest;
        $data = $db->query('SELECT token, key, exp FROM '. DB_PREFIX .'session_auth WHERE id = ? AND user_id = ?', [$payload->id, $payload->user_id]);
        if (!$data->row) {
            return;
        }

        if ($data->row->exp != $payload->exp || $payload->exp <= time()) {
            $data = $db->query('DELETE FROM '. DB_PREFIX .'session_auth WHERE id = ? AND user_id = ?', [$payload->id, $payload->user_id]);
            return;
        }

        if ($this->token !== $data->row->token) {
            return;
        }

        $signature = [
            'alg' => $this->alg,
            'device' => $_SERVER['HTTP_USER_AGENT']
        ];
        $signature64 = $this->base64Encode(json_encode($signature));

        $hash = $this->getHashHmac([$header64, $payload64, $signature64], $data->row->key);
        $hash_token = $this->base64Decode($hash_token);
        if (!hash_equals($hash_token, $hash)) {
            return;
        }

        $this->payload = $payload;
    }
}