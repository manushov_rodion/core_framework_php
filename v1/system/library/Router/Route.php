<?php

namespace system\library\Router;

/**
 * Route class
 * - Класс, который регистрирует доступные маршруты, для роутера.
 * 
 * @author Manushov Rodion <rodion-krox@mail.ru>
 * @version 1.0.0
 */
class Route
{
    /**
     * Содержит массив зарегестрированных маршрутов.
     *
     * @var array
     */
    private $routes = [];

    final public function __construct()
    {
        $this->routes = [
            'GET' => [], 'POST' => [],
            'PUT' => [], 'DELETE' => []
        ];
    }

    /**
     * Метод регистрации GET запросов
     *
     * @param string $url_request
     * @param string $component
     * @param bool $access_public
     * @return void
     */
    final public function get(string $url_request, string $component, bool $access_public = false) : void
    {
        $this->setRoute('GET', $url_request, $component, $access_public);
    }

    /**
     * Метод регистрации POST запросов
     *
     * @param string $url_request
     * @param string $component
     * @param bool $access_public
     * @return void
     */
    final public function post(string $url_request, string $component, bool $access_public = false) : void
    {
        $this->setRoute('POST', $url_request, $component, $access_public);
    }

    /**
     * Метод регистрации PUT запросов
     *
     * @param string $url_request - адрес запроса
     * @param string $component - компонент, который запускается при данном запросе
     * @param bool $access_public - Требуется ли авторизация?
     * @return void
     */
    final public function put(string $url_request, string $component, bool $access_public = false) : void
    {
        $this->setRoute('PUT', $url_request, $component, $access_public);
    }

    /**
     * Метод регистрации DELETE запросов
     *
     * @param string $url_request
     * @param string $component
     * @param bool $access_public
     * @return void
     */
    final public function delete(string $url_request, string $component, bool $access_public = false) : void
    {
        $this->setRoute('DELETE', $url_request, $component, $access_public);
    }

    /**
     * Метод, который регестрирует указанные маршруты, в своей локальной базе
     *
     * @param string $type_request_request
     * @param string $url_request
     * @param string $component
     * @param boolean $access_public
     * @return void
     */
    private function setRoute(string $type_request, string $url_request, string $component, bool $access_public)
    {
        $dataUrlRequest = $this->getDataUrlRequest($url_request);

        if(strpos($component, '@') === false) {
            $component .= '@index'; 
        }

        $data = [
            'component' => $component,
            'access_public' => $access_public,
            'args' => $dataUrlRequest['args']
        ];

        $this->routes[$type_request][$dataUrlRequest['regular']] = $data;
    }

    /**
     * Метод, который возвращает регулярное выражение запроса и наименование аргументов, по данному запросу.
     *
     * @param string $url_request
     * @return array
     */
    private function getDataUrlRequest(string $url_request) : array
    {
        $args = [];

        if (strpos($url_request, '/{') !== false) {
            preg_match_all('/{([a-z0-9_]+)}/', $url_request, $res, PREG_SET_ORDER);

            foreach ($res as $value) {
                $args[] = $value[1];
                $url_request = str_replace($value[0], '([a-z0-9-]+)', $url_request);
            }
        }

        if (strpos($url_request, '{/') !== false) {
            preg_match_all('/{(\/[a-z0-9_]+)}/', $url_request, $res, PREG_SET_ORDER);

            foreach ($res as $value) {
                $args[] = str_replace('/', '', $value[1]);
                $url_request = str_replace($value[0], '/?([a-z0-9-]+)?', $url_request);
            }
        }

        return [
            'regular' => '/^'. str_replace('/', '\/', $url_request) .'\/?$/',
            'args' => $args
        ];
    }

    /**
     * Метод, который возвращает маршруты, доступные по типу запроса.
     *
     * @param string $type_request
     * @return array
     */
    final function getRoutes(string $type_request = '') : array
    {
        if ($type_request) {
            $type_request = strtoupper($type_request);
            return $this->routes[$type_request];
        } else {
            return $this->routes;
        }
    }
}