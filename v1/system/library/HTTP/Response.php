<?php

namespace system\library\HTTP;

/**
 * Response class
 * - Класс, который позволяет подготовить формат ответа и отображает его.
 * 
 * @author Manushov Rodion <rodion-krox@mail.ru>
 * @version 1.0.0
 */
class Response
{
    /**
     * Содержит список заголовок.
     *
     * @var array
     */
    private static $headers = [];

    /**
     * Содержит контент ответа на запрос.
     *
     * @var string
     */
    private $output = '';

    /**
     * Метод, который фиксирует заголовк запроса.
     *
     * @param string $key
     * @param string $value
     * @return void
     */
    final public function setHeader(string $key, string $value) : void
    {
        self::$headers[$key] = $value;
    }

    /**
     * Метод, который удаляет заголовок запроса.
     *
     * @param string $key
     * @return void
     */
    final public function removeHeader(string $key) : void
    {
        if (isset(self::$headers[$key])) {
            unset(self::$headers[$key]);
        }
    }

    /**
     * Метод, который устанавливает контент запроса.
     *
     * @param string $output
     * @return void
     */
    final public function setOutput(string $output) : void
    {
        $this->output = $output;
    }

    /**
     * Метод, который отобращает результат.
     *
     * @return void
     */
    final public function output() : void
    {
        foreach (self::$headers as $key => $value) {
            header($key . ': ' . $value, true);
        }
        echo $this->output;
        exit;
    }
}