<?php

namespace system\library\HTTP;

/**
 * HttpHeader class
 * - Класс, который дает получить все возможные заголовки, отправленные на сервер.
 * 
 * @author Manushov Rodion <rodion-krox@mail.ru>
 * @version 1.0.0
 */
class HttpHeader
{
    /**
     * Содержит заголовки запроса к серверу
     *
     * @var array
     */
    private $headers = [];

    final public function __construct()
    {
        $this->headers = getallheaders();
    }

    /**
     * Проверяет наличие заголовка авторизации [Authorization]
     *
     * @return boolean
     */
    final public function hasAuthorization() : bool
    {
        if (isset($this->headers['Authorization']) && $this->headers['Authorization']) {
            return true;
        }

        return false;
    }

    /**
     * Возвращает значение заголовка по его названию
     *
     * @param string $header_name
     * @return string
     */
    final public function getValue(string $header_name) : string
    {
        if (isset($this->headers[$header_name])) {
            return $this->headers[$header_name];
        }

        return '';
    }

    /**
     * Возвращает перечень заголовков, переданных в при запросе к серверу
     *
     * @return array
     */
    final public function getHeaders() : array
    {
        return $this->$headers;
    }
}