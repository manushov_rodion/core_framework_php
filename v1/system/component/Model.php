<?php

namespace system\component;

use system\library\DataBase\PDORequest;


/**
 * Model class
 * - Базовая модель.
 * 
 * @author Manushov Rodion <rodion-krox@mail.ru>
 * @version 1.0.0
 */
class Model
{
    /**
     * Содержит экземпляр работы с БД
     *
     * @var system\library\DataBase\PDORequest
     */
    protected $db = NULL;

    final public function __construct()
    {
        $this->db = new PDORequest();
    }
}