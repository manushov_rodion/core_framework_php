<?php

namespace system\component;

use system\library\Config;
use system\library\Cache\CacheRequest;


/**
 * View class
 * - Представление, для работые с отображенеим результата.
 * 
 * @author Manushov Rodion <rodion-krox@mail.ru>
 * @version 1.0.0
 */
class View
{
    /**
     * Содержит заголовоки ответа на запрос.
     *
     * @var array
     */
    private $http_headers = [];

    /**
     * Содержит тело ответа на запрос.
     *
     * @var string
     */
    private $http_body = '';

    /**
     * @var system\library\Config
     */
    private $config = null;

    /**
     * Содержит название шаблона
     *
     * @var string
     */
    private $template_file = '';

    /**
     * Содержит наименование шаблона
     *
     * @var string
     */
    private $template_name = '';

    /**
     * @var system\component\Language
     */
    public $language = null;

    /**
     * Содержит адаптер авторизации
     *
     * @var system\library\Auth\AdapterAuth
     */
    public $auth_adapter = null;

    /**
     * @param Config $config
     * @param string $dir_template
     * @param string $template_name
     */
    final public function __construct(Config $config, string $dir_template, string $template_name)
    {
        $this->config = $config;
        $this->template_file = $dir_template . $template_name . '.php';
        $this->template_name = $template_name;
    }

    /**
     * Метод, который заменяет шаблон по умолчанию на пользовательский.
     * - Примечание: Название указывается без префикса View
     *
     * @param string $template_name
     * @return void
     */
    final public function setTemplateName(string $template_name) : void
    {
        $this->template_name = $template_name;
        $this->template_file = $dir_template . $template_name . '.php';
    }

    /**
     * Метод, который устанавливает заголовок ответа
     *
     * @param string $key
     * @param string $value
     * @return void
     */
    public function setHttpHeader(string $key, string $value) : void
    {
        $this->http_headers[$key] = $value;
    }

    /**
     * Метод, который подготавливает и устанавливает содержимое вывода
     *
     * @param stdClass|string $storage
     * @param boolean $off_cache
     * @return void
     */
    public function setHttpBody($storage = null, bool $off_cache = false) : void
    {
        $cache = new CacheRequest;
        $cache->directory = DIR_STORAGE . 'cache/template/' . $this->config->getOption('languages.lang') . '/';
        $cache->main($this->template_name);

        if ($this->config->getOption('view.cache_status') === true && $off_cache === false && $cache->hasCache()) {
            $result = $cache->getCache();
            $this->http_headers = $result['http_headers'];
            $this->http_body = $result['http_body'];
            return;
        }

        if ($storage === null) {
            $storage = new \stdClass;
        }

        if (is_string($storage)) {
            $this->http_body = $storage;
        } else {
            switch ($this->config->getOption('view.type_file')) {
                case 'application/json':
                    $this->http_body = $this->getJson($storage, $off_cache);
                    break;
    
                case 'text/html':
                    $this->http_body = $this->getHTML($storage, $off_cache);
                    break;
    
                default: trigger_error('Указан недопустимый формат HTTP body: ' . $this->config->getOption('view.type_file'), E_USER_ERROR);
            }
        }

        if ($this->config->getOption('view.cache_status') === true && $off_cache === false) {
            $cache->saveCache($this->http_headers, $this->http_body);
        }
    }

    /**
     * Возвращает JSON
     *
     * @param \stdClass $storage
     * @return string
     */
    private function getJson(\stdClass $storage) : string
    {
        return json_encode($storage);
    }

    /**
     * Возвращает шаблон
     *
     * @param \stdClass $storage
     * @return string
     */
    private function getHTML(\stdClass $storage) : string
    {
        return (function(string $file, \stdClass $storage, Language $language) : string {
            $buffer = '';

            if (file_exists($file)) {
                ob_start();
                include $file;
                $buffer = ob_get_contents();
                ob_clean();
            }

            return $buffer;
        })($this->template_file, $storage, $this->language);
    }

    /**
     * Метод, который возвращает результат вызываемого компонента
     *
     * @param string $component_name
     * @param array $args
     * @return string
     */
    final public function loadComponent(string $component_name, array $args = []) : string
    {
        $component = new Component($component_name);
        $component->config = $this->config;
        $component->auth_adapter = $this->auth_adapter;
        $component->args_component = $args;
        [$http_headers, $http_body] = $component->main();
        return $http_body;
    }

    /**
     * Запускает другой контроллер и является заверщающимеся методом
     * - Примечание: после вызова не должен вызываться метод setHttpBody или setHttpHeader
     *
     * @param string $component_name
     * @param array $args
     * @return void
     */
    final public function initComponent(string $component_name, array $args = []) : void
    {
        $component = new Component($component_name);
        $component->config = $this->config;
        $component->auth_adapter = $this->auth_adapter;
        $component->args_component = $args;
        [$this->http_headers, $this->http_body] = $component->main();
    }

    /**
     * Возвращает получившееся данные по View
     *
     * @return array
     */
    final public function getResult() : array
    {
        return [
            'http_headers'   => $this->http_headers,
            'http_body'     => $this->http_body
        ];
    }
}