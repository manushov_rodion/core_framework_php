<?php

namespace system\component;

use system\library\HTTP\Response;


/**
 * Controller class
 * - Базовый контроллер.
 * 
 * @author Manushov Rodion <rodion-krox@mail.ru>
 * @version 1.0.0
 */
class Controller
{
    /**
     * @var system\engine\Model
     */
    public $model = null;

    /**
     * @var system\engine\View
     */
    public $view = null;

    /**
     * @var system\engine\Languages
     */
    public $language = null;

    /**
     * Содержит аргументы, переданные в метод контроллера, при обращение к url
     *
     * @var array
     */
    public $args = [];

    /**
     * Аргументы/Параметры запроса, переданыне через метод запроса
     *
     * @var array
     */
    public $params = [];

    /**
     * Хранилище данных
     *
     * @var \stdClass
     */
    protected $storage = null;

    /**
     * Содержит данные по запросу
     *
     * @var \stdClass
     */
    protected $request = null;

    /**
     * Содержит методы работы, с авторизацией
     *
     * @var ssystem\component\Auth
     */
    public $auth = null;

    final public function __construct()
    {
        $this->storage = new \stdClass;

        $this->request = new \stdClass;
        $this->request->method = strtolower($_SERVER['REQUEST_METHOD']);
        $this->request->url = parse_url( $_SERVER['REQUEST_URI'], PHP_URL_PATH );
    }

    /**
     * Метод, который делает редирект на указанный адрес
     *
     * @param string $url
     * @return void
     */
    final public function redirect(string $url) : void
    {
        $response = new Response();
        $response->setHeader('Location', $url);
        $response->setHeader('HTTP/1.1', '302 Found');
        $response->output();
    }
}