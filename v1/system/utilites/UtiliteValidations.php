<?php

namespace system\utilites;

/**
 * UtiliteValidations class
 * - Класс который содержит валидации.
 * 
 * @author Manushov Rodion <rodion-krox@mail.ru>
 * @version 1.0.0
 */
class UtiliteValidations
{
    /**
     * Главный метод валидации 
     *
     * @param any $value
     * @param string $preg
     * @param string $key
     * @return boolean
     */
    public static function main($value, string $preg, string $key = null) : bool
    {
        if (is_numeric($value)) {
            $value = (string) $value;
        }

        if (!is_string($value)) {
            return false;
        }

        if ($key) {
            $preg .= $key;
        }

        if (preg_match($preg, $value)) {
            return true;
        }

        return false;
    }

    /**
     * Метод, который проверяет валидацию часть адреса запроса
     *
     * @param string $value
     * @param string $key
     * @return boolean
     */
    final public static function isUrlPathRequest($value, string $key = null) : bool
    {
        $preg = '/^[a-z\-0-9]+$/';
        if (strpos($key, 'u') !== false) {
            $preg = '/^[a-zа-я\-0-9]+$/';
        }

        return self::main($value, $preg, $key);
    }

    /**
     * Метод, который проверяет валидацию адреса запроса
     * - Можно указывать домен и субдомен
     *
     * @param string $value
     * @param string $key
     * @return boolean
     */
    final public static function isUrlRequest($value, string $key = null) : bool
    {
        $preg = '/^(https?:\/\/[a-z\.-0-9]+)?\/?([a-z\.-]\/?)+$/';
        if (strpos($key, 'u') !== false) {
            $preg = '/^(https?:\/\/[a-zа-я\.-0-9]+)?\/?([a-zа-я\.-]\/?)+$/';
        }

        return self::main($value, $preg, $key);
    }

    /**
     * Метод, который проверяет валидацию DATETINE UTC
     *
     * @param string $value
     * @param string $key
     * @return boolean
     */
    final public static function isDateTimeUTC($value, string $key = null) : bool
    {
        return self::main($value, '/^[0-9]{4}(-[0-9]{2}){2}T[0-9]{2}(:[0-9]{2}){2}(\.[0-9]+)?Z$/', $key);
    }

    /**
     * Метод, который проверяет валидацию числового ID
     *
     * @param string|int $value
     * @param string $key
     * @return boolean
     */
    final public static function isIntID($value, string $key = null) : bool
    {
        return self::main($value, '/^[0-9]+$/', $key);
    }

    /**
     * Метод, который проверяет валидацию boolean значения
     *
     * @param any $value
     * @return boolean
     */
    final public static function isBool($value) : bool
    {
        if (is_bool($value) || $value == 1 || $value == 0) {
            return true;
        }

        return false;
    }

    /**
     * Метод, который проверяет валидацию на допустимые символы в название
     *
     * @param string $value
     * @param string $key
     * @return boolean
     */
    final public static function isName(string $value, string $key = 'u') : bool
    {
        return self::main($value, '/^[А-Яа-яa-zA-Z0-9ё\/\%\s-\(\)\"\.\'\,\&\№]{2,64}$/', $key);
    }

    /**
     * Метод, который проверяет валидацию на допустимые символы в название
     *
     * @param string|int|float $value
     * @param string $key
     * @return boolean
     */
    final public static function isPrice($value, string $key = null) : bool
    {
        if ($value == 0) {
            return true;
        }

        return self::main($value, '/^(-)?[0-9]+((,|.)[0-9]+)?$/', $key);
    }

    /**
     * Метод, который проверяет валидацию UIID значения
     *
     * @param string $value
     * @param string $key
     * @return boolean
     */
    final public static function isUUID($value, string $key = null) : bool
    {
        return self::main($value, '/^[a-z0-9]{8}(-[a-z0-9]+){3}-[a-z0-9]{12}$/', $key);
    }

    /**
     * Метод, который проверяет валидацию Email значения
     *
     * @param string $value
     * @param string $key
     * @return boolean
     */
    final public static function isEMail($value, string $key = null) : bool
    {
        return self::main($value, '/^([a-z0-9_-]+\.)*[a-z0-9_-]+@[a-z0-9_-]+(\.[a-z0-9_-]+)*\.[a-z]{2,6}$/', $key);
    }

    /**
     * Метод, который проверяет валидацию Password значения
     *
     * @param string $value
     * @param string $key
     * @return boolean
     */
    final public static function isPassword($value, string $key = null) : bool
    {
        return self::main($value, '/(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/', $key);
    }
}