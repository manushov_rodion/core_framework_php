<?php

namespace system\utilites;

/**
 * UtiliteImage class
 * - Класс который .
 * 
 * @author Manushov Rodion <rodion-krox@mail.ru>
 * @version 1.0.0
 */
class UtiliteImage
{
    /**
     * Директория загруженных изображений
     */
    private const DIR_UPLOAD = DIR_STORAGE . 'upload/images/';

    /**
     * Директория ранее измененных изображений
     */
    private const DIR_CACHE = DIR_STORAGE . 'cache/images/';

    /**
     * url загруженных изображений
     */
    private const URL_UPLOAD = URL_STORAGE . 'upload/images/';

    /**
     * url ранее измененных изображений
     */
    private const URL_CACHE = URL_STORAGE . 'cache/images/';


    /**
     * Метод, который изменяет размер изображения и возвращает полный путь до него.
     *
     * @param string $file_name
     * @param integer $width_user_px
     * @param integer $height_user_px
     * @return void
     */
    final public static function resize(string $file_name, int $width_user_px, int $height_user_px) : string
    {
        if (!extension_loaded('gd')) {
			trigger_error('Не подключена библиотека обработки изображения GB! ', E_USER_ERROR);
        }
        
        $file_name = UtiliteCyrToLat::fileName($file_name);
        $extension = pathinfo($file_name, PATHINFO_EXTENSION);

        $fileCache = str_replace('.' . $extension, '', $file_name) . '-' . $width_user_px . 'x' . $height_user_px . '.' . $extension;
        if (file_exists(self::DIR_CACHE . $fileCache)) {
            return self::URL_CACHE . $fileCache;
        }

        if (!file_exists(self::DIR_UPLOAD . $file_name)) {
            return 'нет данного изображения!';
        }

        $fileInfo = getimagesize(self::DIR_UPLOAD . $file_name);
        [$width, $height, $type] = $fileInfo;
        $mime = $fileInfo['mime'];

        if (!in_array($type, [IMAGETYPE_PNG, IMAGETYPE_JPEG, IMAGETYPE_GIF])) { 
            return 'файл не является изображением!';
        }

        $directories = pathinfo($file_name, PATHINFO_DIRNAME);
        if (!file_exists(self::DIR_CACHE . $directories)) {
            mkdir(self::DIR_CACHE . $directories, 0777, true);
        }

        // корректировка рахмеров изображения
        $ratio = $width / $height;
        if ($width_user_px / $height_user_px > $ratio) {
            $width_user_px = $height_user_px * $ratio;
        } else {
            $height_user_px = $width_user_px / $ratio;
        }

        // ресэмплирование
        $newImage = imagecreatetruecolor($width_user_px, $height_user_px);

        if ($mime == 'image/gif') {
            $image = imagecreatefromgif(self::DIR_UPLOAD . $file_name);
        } elseif ($mime == 'image/png') {
            $image = imagecreatefrompng(self::DIR_UPLOAD . $file_name);
        } elseif ($mime == 'image/jpeg') {
            $image = imagecreatefromjpeg(self::DIR_UPLOAD . $file_name);
        }

        if ($mime == 'image/png') {
			imagealphablending($newImage, false);
			imagesavealpha($newImage, true);
			$background = imagecolorallocatealpha($newImage, 255, 255, 255, 127);
			imagecolortransparent($newImage, $background);
		} else {
			$background = imagecolorallocate($newImage, 255, 255, 255);
        }
        
		imagefilledrectangle($newImage, 0, 0, $width_user_px, $height_user_px, $background);
        imagecopyresampled($newImage, $image, 0, 0, 0, 0, $width_user_px, $height_user_px, $width, $height);

        // сохраняем новое изображение
        if ($mime == 'image/gif') {
            imagejpeg($newImage, self::DIR_CACHE . $fileCache, 90);
        } elseif ($mime == 'image/png') {
            imagepng($newImage, self::DIR_CACHE . $fileCache);
        } elseif ($mime == 'image/jpeg') {
            imagegif($newImage, self::DIR_CACHE . $fileCache);
        }

        imagedestroy($newImage);

        return self::URL_CACHE . $fileCache;
    }

    /**
     * Метод, который возвращает полный путь до изображения
     *
     * @param string $file_name
     * @return string
     */
    final public static function getUrlFile(string $file_name) : string
    {
        $file_name = UtiliteCyrToLat::fileName($file_name);

        if (file_exists(self::DIR_UPLOAD . $file_name)) {
            return self::URL_UPLOAD . $file_name;
        }

        return '';
    }
}