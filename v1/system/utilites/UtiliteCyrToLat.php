<?php

namespace system\utilites;

/**
 * UtiliteCyrToLat class
 * - Класс который, конвертирует русские буквы на английские
 * 
 * @author Manushov Rodion <rodion-krox@mail.ru>
 * @version 1.0.0
 */
class UtiliteCyrToLat
{
    /**
     * Метод, который конвертирует названия, с учетом регистра
     *
     * @param string $name
     * @return string
     */
    final public static function name(string $name) : string
    {
        $data = [
            'а' => 'a',   'б' => 'b',   'в' => 'v',
            'г' => 'g',   'д' => 'd',   'е' => 'e',
            'ё' => 'e',   'ж' => 'zh',  'з' => 'z',
            'и' => 'i',   'й' => 'y',   'к' => 'k',
            'л' => 'l',   'м' => 'm',   'н' => 'n',
            'о' => 'o',   'п' => 'p',   'р' => 'r',
            'с' => 's',   'т' => 't',   'у' => 'u',
            'ф' => 'f',   'х' => 'h',   'ц' => 'c',
            'ч' => 'ch',  'ш' => 'sh',  'щ' => 'sch',
            'ь' => '\'',  'ы' => 'y',   'ъ' => '\'',
            'э' => 'e',   'ю' => 'yu',  'я' => 'ya',
            
            'А' => 'A',   'Б' => 'B',   'В' => 'V',
            'Г' => 'G',   'Д' => 'D',   'Е' => 'E',
            'Ё' => 'E',   'Ж' => 'Zh',  'З' => 'Z',
            'И' => 'I',   'Й' => 'Y',   'К' => 'K',
            'Л' => 'L',   'М' => 'M',   'Н' => 'N',
            'О' => 'O',   'П' => 'P',   'Р' => 'R',
            'С' => 'S',   'Т' => 'T',   'У' => 'U',
            'Ф' => 'F',   'Х' => 'H',   'Ц' => 'C',
            'Ч' => 'Ch',  'Ш' => 'Sh',  'Щ' => 'Sch',
            'Ь' => '\'',  'Ы' => 'Y',   'Ъ' => '\'',
            'Э' => 'E',   'Ю' => 'Yu',  'Я' => 'Ya',
        ];

        return strtr($name, $data);
    }

    /**
     * Метод, который конвертирует название файла, с учетом регистра
     *
     * @param string $file_name
     * @return string
     */
    final public static function fileName(string $file_name) : string
    {
        $originalFileName = basename($file_name);
        $newFileName = self::name($originalFileName);
        $newFileName = preg_replace('/[^_a-zA-Z0-9\.]+/u', '_', $newFileName);
        $newFileName =  trim($newFileName, '_');
        return str_replace($originalFileName, $newFileName, $file_name);
    }

    /**
     * Метод, который конвертирует url и преобразовывает его в нижний регистр
     *
     * @param string $url
     * @return string
     */
    final public static function url(string $url) : string
    {
        $url = self::name($url);
        $url = strtolower($url);
        $url = preg_replace('/[^-a-z0-9_]+/u', '-', $url);
        return trim($url, '-');
    }
}