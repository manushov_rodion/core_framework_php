<!DOCTYPE html>
<html lang="<?php echo $language->lang ?>">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta http-equiv="X-UA-Compatible" content="ie=edge">

	<?php if (isset($storage->css)) : foreach ($storage->css as $css) : ?>
		<link rel="stylesheet" href="<?php echo $css['url'] ?>?version=<?php echo $css['version'] ?>">
	<?php endforeach; endif ?>

		<title><?php echo $storage->title ?></title>
	</head>
	<body>
		<?php if (isset($storage->nav)) : ?>
			<nav>
				<?php foreach ($storage->nav as $nav) : ?>
					<a href="<?php echo $nav['link'] ?>"><?php echo $nav['name'] ?></a>
					<span> # </span>
				<?php endforeach ?>
			</nav>
		<?php endif ?>