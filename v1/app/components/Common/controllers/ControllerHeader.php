<?php

namespace components\Common\controllers;

use system\component\Controller;


/**
 * ControllerHeader class
 * 
 * @author Manushov Rodion <rodion-krox@mail.ru>
 * @version 1.0.0
 */
class ControllerHeader extends Controller
{
    public function index()
    {
		$this->storage->css = [
			[ 'url' => URL_ASSETS . 'css/main.css', 'version' => '1.0.0' ]
		];

		$this->storage->title = DOMAIN_NAME;
		if ($this->args->title) {
			$this->storage->title = $this->args->title;
		}

		$this->storage->nav = [
			[ 'link' => '/demo', 'name' => 'page: demo' ],
			[ 'link' => '/demo-no-page', 'name' => 'page: no in the database' ],
			[ 'link' => '/no-access-public', 'name' => 'page: no access public' ],
			[ 'link' => '/redirect', 'name' => 'page: redirect' ],
			[ 'link' => '/demo/demo', 'name' => 'page: 404' ],
			[ 'link' => '/custom', 'name' => 'page: custom' ],
		];

    	$this->view->setHttpBody($this->storage);
    }
}