<?php

namespace components\Pages\Controllers;

use system\component\Controller;


/**
 * ControllerFrontPage class
 * 
 * @author Manushov Rodion <rodion-krox@mail.ru>
 * @version 1.0.0
 */
class ControllerFrontPage extends Controller
{
    public function index()
    {
        $this->storage->header = $this->view->loadComponent('Common.Header@index', ['title' => 'pages']);
        $this->storage->footer = $this->view->loadComponent('Common.Footer@index');

        $this->storage->title = $this->language->get('DEMO FRONT PAGE');

        $this->view->setHttpBody($this->storage);
    }
}