<?php

namespace components\Pages\Controllers;

use system\component\Controller;


/**
 * ControllerPageNoAccess class
 * 
 * @author Manushov Rodion <rodion-krox@mail.ru>
 * @version 1.0.0
 */
class ControllerPageNoAccess extends Controller
{
    public function index()
    {
        if ($this->request->method === 'post') {
            $this->auth->delete('7331ec0a-5de3-49e4-aa93-3b125169b93b');
            $this->redirect('/');
        }

        $this->storage->header = $this->view->loadComponent('Common.Header@index', ['title' => 'pages']);
        $this->storage->footer = $this->view->loadComponent('Common.Footer@index');

        $this->storage->title = $this->language->get('DEMO NO ACCESS PAGE');

        $this->view->setHttpBody($this->storage);
    }
}