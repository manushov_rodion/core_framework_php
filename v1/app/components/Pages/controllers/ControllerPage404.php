<?php

namespace components\Pages\Controllers;

use system\component\Controller;


/**
 * ControllerPage404 class
 * 
 * @author Manushov Rodion <rodion-krox@mail.ru>
 * @version 1.0.0
 */
class ControllerPage404 extends Controller
{
    public function index()
    {
        $this->storage->header = $this->view->loadComponent('Common.Header@index', ['title' => 'pages']);
        $this->storage->footer = $this->view->loadComponent('Common.Footer@index');

        $this->storage->img = URL_ASSETS . 'img/404.png';

        $this->view->setHttpHeader('HTTP/1.1', '404 Not Found');
        $this->view->setHttpBody($this->storage, true);
    }
}