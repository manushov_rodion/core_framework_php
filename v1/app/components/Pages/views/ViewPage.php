<?php echo $storage->header ?>

<h1><?php echo $storage->title ?></h1>

<?php if($storage->images) : ?>
    <img src="<?php echo $storage->images['img_jpg'] ?>" alt="">
    <img src="<?php echo $storage->images['original_jpg'] ?>" alt="">
    <hr>
    <img src="<?php echo $storage->images['img_png'] ?>" alt="">
    <img src="<?php echo $storage->images['original_png'] ?>" alt="">
    <hr>
    <img src="<?php echo $storage->images['img_gif'] ?>" alt="">
    <img src="<?php echo $storage->images['original_gif'] ?>" alt="">
<?php endif ?>

<?php echo $storage->footer ?>