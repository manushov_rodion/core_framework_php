<?php echo $storage->header ?>

<br>
<br>
<br>

<form action="/auth" method="POST">

    <div>
        <label><?php $language->the('Логин') ?></label>
        <input type="email" name="login">
        <?php if (isset($storage->errors['login']) && $storage->errors['login']) : ?>
            <p><?php echo $storage->errors['login'] ?></p>
        <?php endif ?>
    </div>

    <br>

    <div>
        <label><?php $language->the('Пароль') ?></label>
        <input type="password" name="password">
        <?php if (isset($storage->errors['password']) && $storage->errors['password']) : ?>
            <p><?php echo $storage->errors['password'] ?></p>
        <?php endif ?>
    </div>

    <br>

    <input type="submit" value="<?php $language->the('Авторизоваться') ?>">
    <?php if (isset($storage->errors['all']) && $storage->errors['all']) : ?>
        <p><?php echo $storage->errors['all'] ?></p>
    <?php endif ?>

</form>

<?php echo $storage->footer ?>