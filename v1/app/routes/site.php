<?php

/**
 * Файл списка доступных маршрутов.
 * 
 * @author Manushov Rodion <rodion-krox@mail.ru>
 * @version 1.0.0
 * 
 * @param string $url_request
 * @param string $component
 * @param bool $access_public
 * @return void
 * 
 * $route->get(...);
 * $route->post(...);
 * $route->put(...);
 * $route->delete(...);
 */

$route->get('/{page}', 'Pages.Page@index', true);
$route->get('/', 'Pages.FrontPage@index', true);

$route->get('/auth', 'Pages.PageAuth@index', true);
$route->post('/auth', 'Pages.PageAuth@index', true);

$route->get('/no-access-public', 'Pages.PageNoAccess@index');
$route->post('/no-access-public', 'Pages.PageNoAccess@index');

$route->get('/custom', 'Pages.PageСustom@index', true);
